/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Screen;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Guitazr
 */
public class ShowAddFrame extends javax.swing.JFrame {

    /**
     * Creates new form ShowAddFrame
     */
    public ShowAddFrame() {
        initComponents();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        usernameInput = new javax.swing.JTextField();
        passwordInput = new javax.swing.JTextField();
        firstnameInput = new javax.swing.JTextField();
        lastnameInput = new javax.swing.JTextField();
        weightInput = new javax.swing.JTextField();
        heightInput = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("Username:");

        jLabel3.setText("Password:");

        jLabel4.setText("Firstname:");

        jLabel5.setText("Lastname:");

        jLabel7.setText("Height:");

        usernameInput.setPreferredSize(new java.awt.Dimension(6, 35));

        passwordInput.setPreferredSize(new java.awt.Dimension(6, 35));

        firstnameInput.setPreferredSize(new java.awt.Dimension(6, 35));

        lastnameInput.setPreferredSize(new java.awt.Dimension(6, 35));

        weightInput.setPreferredSize(new java.awt.Dimension(6, 35));

        heightInput.setPreferredSize(new java.awt.Dimension(6, 35));

        jLabel6.setText("Weight:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(weightInput, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                    .addComponent(usernameInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(passwordInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(firstnameInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lastnameInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(heightInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(usernameInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(passwordInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(firstnameInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lastnameInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(weightInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(heightInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(159, Short.MAX_VALUE))
        );

        jButton2.setText("Back");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 102, 204));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Add User");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(289, 289, 289))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(143, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(114, 114, 114))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        sayAdd();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        sayMenu();
    }//GEN-LAST:event_jButton2ActionPerformed

    public void sayMenu() {
        this.setVisible(false);
        this.dispose();
        ShowMenuFrame fram = new ShowMenuFrame();
        fram.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        Data.load();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShowAddFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField firstnameInput;
    private javax.swing.JTextField heightInput;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField lastnameInput;
    private javax.swing.JTextField passwordInput;
    private javax.swing.JTextField usernameInput;
    private javax.swing.JTextField weightInput;
    // End of variables declaration//GEN-END:variables


    /*public boolean chooseAddUser() {
        String regex = "^[a-z ,.'-]+$";
        if (!firstnameInput.getText().matches(regex)) {
            return false;
        } else {
            return true;
        }
    }*/
    
    public boolean chooseAddFname() {
        String regex = "^[a-z ,.'-]+$";
        if (!firstnameInput.getText().matches(regex)) {
            return false;
        } else {
            return true;
        }
    }
    
    public boolean chooseAddLname() {
        String regex = "^[a-z ,.'-]+$";
        if (!lastnameInput.getText().matches(regex)) {
            return false;
        } else {
            return true;
        }
    }
    
    public boolean chooseAddWeight() {
        String regex = "\\d+\\.\\d*|\\.?\\d+";
        if (!weightInput.getText().matches(regex)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean chooseAddHeight() {
        String regex = "\\d+\\.\\d*|\\.?\\d+";
        if (!heightInput.getText().matches(regex)) {
            return false;
        } else {
            return true;
        }
    }
    
    
    private boolean chooseAddUser() {
        ArrayList<User>array = Data.userlist;
        for(int i=0 ; i<array.size() ; i++){
          
            if(usernameInput.getText().equals(array.get(i).getUsername())){
                System.out.print(array.get(i).getId());
                return false;
            }
        }
        return true;
    }
    /*
    private boolean checkWeight() {
        while(true){
            try {
            String s = weightInput.getText().toString();
            int n = Integer.parseInt(s);
            return true;
            }
            catch(Exception e) {
                break;
            }
        }
        return false;
    }
    
    private boolean checkHeight() {
        while(true){
            try {
            String s = heightInput.getText().toString();
            int n = Integer.parseInt(s);
            return true;
            }
            catch(Exception e) {
                break;
            }
        }
        return false;
    }*/
    
    public void showErrorAddName() {
        JOptionPane.showMessageDialog(null, "Name must be String!", "alert", JOptionPane.ERROR_MESSAGE);
    }
    
    public void showErrorAddHeight() {
        JOptionPane.showMessageDialog(null, "Height must be number!", "alert", JOptionPane.ERROR_MESSAGE);
    }

    public void showErrorAddWeight() {
        JOptionPane.showMessageDialog(null, "Weight must be number!", "alert", JOptionPane.ERROR_MESSAGE);
    }
    
    private void showErrorUserExist() {
        JOptionPane.showMessageDialog(null, "Username have exist!", "Error", JOptionPane.WARNING_MESSAGE);
    }
    
    private void showErrorInformationNotCmplete() {
        JOptionPane.showMessageDialog(null, "Please fill out all information!", "Error", JOptionPane.WARNING_MESSAGE);
    }

    private void showComplete() {
        int indexID = Data.userlist.get(Data.userlist.size()-1).getId();
        Data.userlist.add(new User(indexID+1,usernameInput.getText(), passwordInput.getText(),  firstnameInput.getText(), lastnameInput.getText() ,Integer.parseInt( weightInput.getText()),Integer.parseInt( heightInput.getText())));
        JOptionPane.showMessageDialog(this,"success");
        
    }
    

    private void sayAdd() {
       if(usernameInput.getText().toString().isEmpty() ||
                passwordInput.getText().toString().isEmpty() ||
                firstnameInput.getText().toString().isEmpty() ||
                lastnameInput.getText().toString().isEmpty() ||
                weightInput.getText().toString().isEmpty() ||
                heightInput.getText().toString().isEmpty()
                ){
            showErrorInformationNotCmplete();
        }
       
        else if(!chooseAddUser()){
            showErrorUserExist();
        }
        else if(!chooseAddFname()){
            showErrorAddName();
        }
        else if(!chooseAddLname()){
            showErrorAddName();
        }
        else if(!chooseAddWeight()){
            showErrorAddWeight();
        }
        else if(!chooseAddHeight()){
            showErrorAddHeight();
        }
        else{
            showComplete();
        }//To change body of generated methods, choose Tools | Templates.
    }
}
